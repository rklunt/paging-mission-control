package missioncontrol;

import com.google.gson.GsonBuilder;
import missioncontrol.data.AlertMessage;
import missioncontrol.data.AlertMessageSerializer;
import missioncontrol.data.TelemetryData;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;

@SpringBootApplication
public class PagingMissionControlApp implements CommandLineRunner {

    private final TelemetryAlertService telemetryAlertService = new TelemetryAlertServiceImpl();

    public static void main(String[] args) {
        SpringApplication springApp = new SpringApplication(PagingMissionControlApp.class);
        springApp.setBannerMode(Banner.Mode.OFF);
        springApp.run(args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<TelemetryData> telemetryData = parseTelemetryFile(args[0]);
        List<AlertMessage> alertMessages = telemetryAlertService.detectAlerts(telemetryData);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AlertMessage.class, new AlertMessageSerializer());
        gsonBuilder.setPrettyPrinting();
        System.out.println(gsonBuilder.create().toJson(alertMessages));
    }

    static List<TelemetryData> parseTelemetryFile(String filename) {
        List<TelemetryData> telemetryList = new ArrayList<>();
        try {
            File telemetryFile = new File(filename);
            Scanner scanner = new Scanner(telemetryFile);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (!line.trim().isEmpty()) {
                    String[] telemetryParts = line.split("\\|");
                    telemetryList.add(new TelemetryData(
                            parseInstantFromTelemetry(telemetryParts[0]),
                            parseInt(telemetryParts[1]),
                            parseDouble(telemetryParts[2]),
                            parseDouble(telemetryParts[3]),
                            parseDouble(telemetryParts[4]),
                            parseDouble(telemetryParts[5]),
                            parseDouble(telemetryParts[6]),
                            TelemetryData.Component.valueOf(telemetryParts[7])
                    ));
                }
            }
        } catch (IOException e) {
            System.out.println("An error occurred reading telemetry data from file: " + e.getMessage());
        }
        return telemetryList;
    }

    private static Instant parseInstantFromTelemetry(String dateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd kk:mm:ss.SSS");
        LocalDateTime parsedDateTime = LocalDateTime.parse(dateTime, formatter);
        OffsetDateTime offsetDateTime = OffsetDateTime.of(parsedDateTime, ZoneOffset.UTC);
        return Instant.from(offsetDateTime);
    }
}
