package missioncontrol;

import missioncontrol.data.AlertMessage;
import missioncontrol.data.TelemetryData;

import java.util.List;

public interface TelemetryAlertService {
    List<AlertMessage> detectAlerts(List<TelemetryData> telemetryDataList);
}
