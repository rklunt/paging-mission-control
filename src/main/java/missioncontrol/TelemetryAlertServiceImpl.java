package missioncontrol;

import missioncontrol.data.AlertMessage;
import missioncontrol.data.TelemetryData;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static missioncontrol.data.AlertMessage.SEVERITY_RED_HIGH;
import static missioncontrol.data.AlertMessage.SEVERITY_RED_LOW;
import static missioncontrol.data.TelemetryData.Component.BATT;
import static missioncontrol.data.TelemetryData.Component.TSTAT;

public class TelemetryAlertServiceImpl implements TelemetryAlertService {

    private static final long FIVE_MINUTES = 300000L;

    private static class DetectedAlert {
        private final Instant timestamp;
        private int count = 1;
        private DetectedAlert(Instant timestamp) {
            this.timestamp = timestamp;
        }
    }

    @Override
    public List<AlertMessage> detectAlerts(List<TelemetryData> telemetryDataList) {
        Map<Integer, DetectedAlert> lowVoltageAlerts = new HashMap<>();
        Map<Integer, DetectedAlert> highTempAlerts = new HashMap<>();
        List<AlertMessage> alertMessages = new ArrayList<>();

        for (TelemetryData telemetryData : telemetryDataList) {
            int satelliteId = telemetryData.getSatelliteId();

            if (telemetryData.getComponent() == BATT && telemetryData.getRawValue() < telemetryData.getRedLowLimit()) {
                DetectedAlert detectedAlert = lowVoltageAlerts.get(satelliteId);
                if (detectedAlert == null || (telemetryData.getTimestamp().toEpochMilli() - detectedAlert.timestamp.toEpochMilli()) > FIVE_MINUTES) {
                    detectedAlert = new DetectedAlert(telemetryData.getTimestamp());
                    lowVoltageAlerts.put(satelliteId, detectedAlert);
                } else {
                    detectedAlert.count++;
                }
                if (detectedAlert.count == 3) {
                    alertMessages.add(new AlertMessage(satelliteId, SEVERITY_RED_LOW, BATT, detectedAlert.timestamp));
                    lowVoltageAlerts.remove(satelliteId);
                }
            }

            if (telemetryData.getComponent() == TSTAT && telemetryData.getRawValue() > telemetryData.getRedHighLimit()) {
                DetectedAlert detectedAlert = highTempAlerts.get(satelliteId);
                if (detectedAlert == null || (telemetryData.getTimestamp().toEpochMilli() - detectedAlert.timestamp.toEpochMilli()) > FIVE_MINUTES) {
                    detectedAlert = new DetectedAlert(telemetryData.getTimestamp());
                    highTempAlerts.put(satelliteId, detectedAlert);
                } else {
                    detectedAlert.count++;
                }
                if (detectedAlert.count == 3) {
                    alertMessages.add(new AlertMessage(satelliteId, SEVERITY_RED_HIGH, TSTAT, detectedAlert.timestamp));
                    highTempAlerts.remove(satelliteId);
                }
            }
        }
        return alertMessages;
    }
}
