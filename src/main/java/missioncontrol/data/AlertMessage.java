package missioncontrol.data;

import java.time.Instant;
import java.util.Objects;

public class AlertMessage {

    public static final String SEVERITY_RED_LOW = "RED LOW";
    public static final String SEVERITY_RED_HIGH = "RED HIGH";
    private final Integer satelliteId;
    private final String severity;
    private final TelemetryData.Component component;
    private final Instant timestamp;

    public AlertMessage(Integer satelliteId, String severity, TelemetryData.Component component, Instant timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof AlertMessage)) {
            return false;
        }
        AlertMessage other = (AlertMessage) obj;
        return Objects.equals(satelliteId, other.satelliteId)
                && Objects.equals(severity, other.severity)
                && Objects.equals(component, other.component)
                && Objects.equals(timestamp, other.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(satelliteId, severity, component, timestamp);
    }

    public Integer getSatelliteId() {
        return satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public TelemetryData.Component getComponent() {
        return component;
    }

    public Instant getTimestamp() {
        return timestamp;
    }
}
