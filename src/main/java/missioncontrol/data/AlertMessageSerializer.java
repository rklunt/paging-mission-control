package missioncontrol.data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;

public class AlertMessageSerializer implements JsonSerializer<AlertMessage> {
    @Override
    public JsonElement serialize(AlertMessage src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject alertMessageJson = new JsonObject();
        alertMessageJson.addProperty("satelliteId", src.getSatelliteId());
        alertMessageJson.addProperty("severity", src.getSeverity());
        alertMessageJson.addProperty("component", src.getComponent().name());
        alertMessageJson.addProperty("timestamp", ISO_INSTANT.format(src.getTimestamp()));
        return alertMessageJson;
    }
}
