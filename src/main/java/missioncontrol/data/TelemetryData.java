package missioncontrol.data;

import java.time.Instant;
import java.util.Objects;

public final class TelemetryData {
    private final Instant timestamp;
    private final int satelliteId;
    private final Double redHighLimit;
    private final Double yellowHighLimit;
    private final Double yellowLowLimit;
    private final Double redLowLimit;
    private final Double rawValue;
    private final Component component;

    public enum Component { TSTAT, BATT }

    public TelemetryData(Instant timestamp, int satelliteId, Double redHighLimit, Double yellowHighLimit,
                         Double yellowLowLimit, Double redLowLimit, Double rawValue, Component component) {
        this.timestamp = timestamp;
        this.satelliteId = satelliteId;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof TelemetryData)) {
            return false;
        }
        TelemetryData other = (TelemetryData)obj;
        return Objects.equals(timestamp, other.timestamp)
                && Objects.equals(satelliteId, other.satelliteId)
                && Objects.equals(redHighLimit, other.redHighLimit)
                && Objects.equals(yellowHighLimit, other.yellowHighLimit)
                && Objects.equals(yellowLowLimit, other.yellowLowLimit)
                && Objects.equals(redLowLimit, other.redLowLimit)
                && Objects.equals(rawValue, other.rawValue)
                && Objects.equals(component, other.component);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, satelliteId, redHighLimit, yellowHighLimit, yellowLowLimit, redLowLimit, rawValue, component);
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public Double getRedHighLimit() {
        return redHighLimit;
    }

    public Double getYellowHighLimit() {
        return yellowHighLimit;
    }

    public Double getYellowLowLimit() {
        return yellowLowLimit;
    }

    public Double getRedLowLimit() {
        return redLowLimit;
    }

    public Double getRawValue() {
        return rawValue;
    }

    public Component getComponent() {
        return component;
    }

}
