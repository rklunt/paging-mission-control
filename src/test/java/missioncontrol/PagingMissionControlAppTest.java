package missioncontrol;

import missioncontrol.data.TelemetryData;
import org.junit.Test;

import java.time.Instant;
import java.util.List;

import static missioncontrol.PagingMissionControlApp.parseTelemetryFile;
import static missioncontrol.data.TelemetryData.Component.BATT;
import static missioncontrol.data.TelemetryData.Component.TSTAT;
import static org.junit.Assert.assertEquals;

public class PagingMissionControlAppTest {

    @Test
    public void testParseTelemetryFile() {
        List<TelemetryData> telemetryData = parseTelemetryFile("src/test/resources/telemetryData.txt");
        assertEquals(14, telemetryData.size());
        assertEquals(new TelemetryData(Instant.parse("2018-01-01T23:01:05.001Z"), 1001, 101.0, 98.0, 25.0, 20.0, 99.9, TSTAT), telemetryData.get(0));
        assertEquals(new TelemetryData(Instant.parse("2018-01-01T23:05:07.421Z"), 1001, 17.0, 15.0, 9.0, 8.0, 7.9, BATT), telemetryData.get(13));
    }
}