package missioncontrol;

import missioncontrol.data.AlertMessage;
import missioncontrol.data.TelemetryData;
import org.junit.Test;

import java.time.Instant;
import java.util.List;

import static java.time.format.DateTimeFormatter.ISO_INSTANT;
import static missioncontrol.data.TelemetryData.Component.BATT;
import static missioncontrol.data.TelemetryData.Component.TSTAT;
import static org.junit.Assert.assertEquals;

public class TelemetryAlertServiceImplTest {

    @Test
    public void testDetectAlerts() {
        List<TelemetryData> telemetryData = PagingMissionControlApp.parseTelemetryFile("src/test/resources/telemetryData.txt");
        TelemetryAlertService alertService = new TelemetryAlertServiceImpl();
        List<AlertMessage> alertMessages = alertService.detectAlerts(telemetryData);
        assertEquals(2, alertMessages.size());
        assertEquals(new AlertMessage(1000, AlertMessage.SEVERITY_RED_HIGH, TSTAT, Instant.from(ISO_INSTANT.parse("2018-01-01T23:01:38.001Z"))), alertMessages.get(0));
        assertEquals(new AlertMessage(1000, AlertMessage.SEVERITY_RED_LOW, BATT, Instant.from(ISO_INSTANT.parse("2018-01-01T23:01:09.521Z"))), alertMessages.get(1));
    }

    @Test
    public void testDetectAlertsWithTstatAlertsForTwoSatellites() {
        List<TelemetryData> telemetryData = PagingMissionControlApp.parseTelemetryFile("src/test/resources/telemetryData2.txt");
        TelemetryAlertService alertService = new TelemetryAlertServiceImpl();
        List<AlertMessage> alertMessages = alertService.detectAlerts(telemetryData);
        assertEquals(3, alertMessages.size());
        assertEquals(new AlertMessage(1000, AlertMessage.SEVERITY_RED_HIGH, TSTAT, Instant.from(ISO_INSTANT.parse("2018-01-01T23:01:38.001Z"))), alertMessages.get(0));
        assertEquals(new AlertMessage(1001, AlertMessage.SEVERITY_RED_HIGH, TSTAT, Instant.from(ISO_INSTANT.parse("2018-01-01T23:01:26.011Z"))), alertMessages.get(1));
        assertEquals(new AlertMessage(1000, AlertMessage.SEVERITY_RED_LOW, BATT, Instant.from(ISO_INSTANT.parse("2018-01-01T23:01:09.521Z"))), alertMessages.get(2));
    }
}