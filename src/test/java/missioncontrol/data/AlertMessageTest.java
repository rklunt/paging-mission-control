package missioncontrol.data;

import com.google.gson.GsonBuilder;
import org.junit.Test;

import java.time.Instant;

import static java.lang.String.format;
import static missioncontrol.data.AlertMessage.SEVERITY_RED_HIGH;
import static missioncontrol.data.TelemetryData.Component.TSTAT;
import static org.junit.Assert.assertEquals;

public class AlertMessageTest {

    private static final Integer SATELLITE_ID = 1;
    private static final TelemetryData.Component COMPONENT = TSTAT;
    private static final String TIME_STRING = "2021-03-27T15:42:02.280Z";
    private static final Instant TIMESTAMP = Instant.parse(TIME_STRING);

    @Test
    public void testAlertMessageToJson() {
        AlertMessage alertMessage = new AlertMessage(SATELLITE_ID, SEVERITY_RED_HIGH, COMPONENT, TIMESTAMP);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AlertMessage.class, new AlertMessageSerializer());
        String expectedJson = format("{\"satelliteId\":%d,\"severity\":\"%s\",\"component\":\"%s\",\"timestamp\":\"%s\"}",
                SATELLITE_ID, SEVERITY_RED_HIGH, COMPONENT, TIME_STRING);
        assertEquals(gsonBuilder.create().toJson(alertMessage), expectedJson);
    }

}